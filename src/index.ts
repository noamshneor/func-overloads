import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

// const response = saySomething("hello");
// log.magenta(response);

/* eslint-disable */
//---------------------------------------------
//     Function overloads challenge
//---------------------------------------------
/**
 * Implement a function called *random* which returns a random number
 * we can optionaly pass a *max* range to pick from
 * we can optionaly pass a *min* number to start the range
 * we can optionally pass a *float* boolean to indicate if we want a float (true) or an integer (false)
 *   the *default is false* for the float boolean
 * if we call random with *no parameters*, it should *return either 0 or 1*
 * -----------------------
 * implement the function and call it in all variations
 * log the results for the different use cases
 */

function random(arg1?: boolean, arg2?: number, arg3?: number) {
    if (typeof arg1 === 'boolean') {
        if (typeof arg2 === 'number') {
            if (typeof arg3 === 'number') {
                if (arg1) return (Math.random() * (arg3 - arg2)) + arg2;
                return Math.floor((Math.random() * (arg3 - arg2)) + arg2);
            }
            if (arg1) return Math.random() * (arg2);
            return Math.floor(Math.random() * (arg2 + 1));
        }
        if (arg1) return Math.random();
    }
    return Math.round(Math.random());
}

const num1 = random(); // 0 ~ 1 | integer
const num2 = random(true); // 0 ~ 1 | float
const num3 = random(false, 6); // 0 ~ 6 | integer,
const num4 = random(false, 2, 6); // 2 ~ 6 | integer
const num5 = random(true, 6); // 0 ~ 6 | float
const num6 = random(true, 2, 6); // 2 ~ 6 | float

console.log({ num1, num2, num3, num4, num5, num6 });

//---------------------------------------------------------------
